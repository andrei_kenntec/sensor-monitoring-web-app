This is a basic web app to run on a raspberry pi.

At this point it's pretty much directly from 
https://www.youtube.com/watch?v=UTwns9Oq1No

For details on installing and getting running refer to 
https://kenntec.atlassian.net/wiki/spaces/RD/pages/42827919/Getting+Rasberry+Pi+Sensor+module+going
(if you don't want to follow through the tutorial and understand it in detail)

You can load data from either DHT11 or DHT22 sensors.
plan is that more sensor types can be added later.

Load the sesnors into the database using either the "Sensors" table in dht.db (And SQLite database)
or add them via the GUI in the web app 

will take a reading every ten minutes from each sensor and log in the Readings table of the database
Readings can be viewed via the web app.

Can also export readings to csv for excell analysis